package com.example.davaleba10

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import android.view.LayoutInflater
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintSet
import androidx.viewpager.widget.ViewPager
import com.example.davaleba10.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViewPager()

    }
    private fun initViewPager(){
        val layouts : Array<Int> = arrayOf(R.layout.image_layout1, R.layout.image_layout2, R.layout.image_layout3, R.layout.image_layout4, R.layout.image_layout5)

        binding.viewPager.adapter = viewAdapter(this,layouts)
    }
    private fun changeImage(position : Int){

    }
}